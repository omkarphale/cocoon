#include "timehelper.h"

static LARGE_INTEGER previousTime;
static LARGE_INTEGER currentTime;
static LARGE_INTEGER tempTime;
static double freq = 0.0f;

void InitClock(void)
{
    // get the frequency from OS
    QueryPerformanceFrequency(reinterpret_cast<LARGE_INTEGER*>(&tempTime));
    freq = (static_cast<double>(tempTime.QuadPart)) / 0.001;

    QueryPerformanceCounter(&previousTime);
    currentTime = previousTime;
}

float GetCurrentTimeMilliSeconds(void)
{
	QueryPerformanceCounter(&tempTime);
	tempTime.QuadPart *= 10000;
	tempTime.QuadPart /= (LONGLONG)freq;

	return (float)tempTime.QuadPart;
}
