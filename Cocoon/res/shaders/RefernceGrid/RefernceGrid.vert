#version 460 core                                                                                    
                        
in vec4 v_position;  

out vec4 v_out_color;         

uniform mat4 u_projection_matrix;
uniform mat4 u_view_matrix;
uniform mat4 u_model_matrix;
                        
void main(void)                                                                                      
{                                                                                                    
   v_out_color = v_position;

   gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * v_position;          
}                                                                                                     
