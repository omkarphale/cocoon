#version 460 core                 
                        
in vec4 v_out_color;                                                                                   
                        
out vec4 FragmentColor;                                                                                                       
                                                                                                                            
void main(void)                                                                                                               
{                  
    FragmentColor = vec4(v_out_color.rgb, 1.0f);
}                                                                                                                              
