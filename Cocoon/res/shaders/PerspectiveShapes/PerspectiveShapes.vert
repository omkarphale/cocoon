#version 460 core												
																
in vec4 v_position;												
in vec4 v_color;												
																
uniform mat4 u_view_matrix;										
uniform mat4 u_model_matrix;										
uniform mat4 u_projection_matrix;										
																
out vec4 out_v_color;											
																
void main(void)													
{																	
	out_v_color = v_color;											
	gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * v_position;						
}																	 
