#version 460 core

in vec4 out_v_color;

out vec4 FragmentColor;

void main(void)
{
	FragmentColor = out_v_color;	
}
