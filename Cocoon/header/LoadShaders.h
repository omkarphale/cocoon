#pragma once
#include "Standard.h"
#include "GraphicsHelper.h"

typedef struct BIND_ATTRIBUTES_INFO
{
    const char* attribute;
    GLuint index;

} BIND_ATTRIBUTES_INFO;

#define CREATE_PROGRAM_SHADER_LOAD_FROM_FILE 0x0
#define CREATE_PROGRAM_SHADER_LOAD_FROM_STRING 0x1

typedef struct SHADERS_INFO
{
    GLenum shaderType;
    GLenum shaderLoadAs;
    union
    {
        const char* shaderSource;
        const char* shaderFileName;
    };

    GLuint shaderID;
} SHADERS_INFO;

// function declaration
GLuint createProgram(SHADERS_INFO* shaderInfo, 
                    int shaderCount, 
                    BIND_ATTRIBUTES_INFO* attribInfo, 
                    int attribCount, 
                    int lineNo, 
                    int num_varyings = 0, 
                    const char** varyings_names = NULL,
                    GLenum transformFeedbackBufferMode = GL_SEPARATE_ATTRIBS);
void deleteProgram(GLuint program);

