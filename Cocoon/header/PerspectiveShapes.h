#pragma once
#include "Standard.h"
#include "GraphicsHelper.h"
#include "LoadShaders.h"
#include "SceneBuffers.h"
#include "Logger.h"
#include "Scenes.h"
#include "Sphere.h"

class PerspectiveShapes : public Scenes
{
private:
	GLuint m_shaderProgramObject = 0;
	GLfloat m_rotationAngle = 0.0f;
	GLfloat m_rotationFactor = ROTATION_FACTOR;

	SHADERS_INFO m_shaderInfo[2] = { 0 };

	// Array Of Shapes
	// Triangle
	const GLfloat triangleVertices[9] = {
		+0.0f, +1.0f, 0.0f,		
		-1.0f, -1.0f, 0.0f,		
		+1.0f, -1.0f, 0.0f };	

	const GLfloat triangleColors[9] = {
		1.0f, 0.0f, 0.0f,		
		0.0f, 1.0f, 0.0f,		
		0.0f, 0.0f, 1.0f };		

	// Quad
	const GLfloat quadVertices[18] = {
		+1.0f, +1.0f, 0.0f,		
		-1.0f, +1.0f, 0.0f,		
		-1.0f, -1.0f, 0.0f,	
		
		+1.0f, +1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
		+1.0f, -1.0f, 0.0f };	

	const GLfloat quadColors[18] = {
		1.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 1.0f,
		
		1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 1.0f, 0.0f };

public:
	PerspectiveShapes();
	~PerspectiveShapes();

	void initialize(void);
	void display(const mat4*);
	void update(void);
	void uninitialize(void);

	void CreateShapeBuffers(SceneBuffers* buffer = nullptr, GLuint* vao = nullptr, GLuint* vbo = nullptr, const int arraySize = 0, const GLfloat* arrayValues = nullptr, const int attributeLocation = 0, const int stride = 0, const GLenum usage = GL_STATIC_DRAW, const GLenum type = GL_FLOAT);
	void DrawShapesUsingArray(SceneBuffers* buffer, const int count, const mat4 viewMatrix, const mat4 modelMatrix, const mat4 projectionMatrix);
	void DrawShapesUsingElements(SceneBuffers* buffer, const int count, const mat4 viewMatrix, const mat4 modelMatrix, const mat4 projectionMatrix);
};
