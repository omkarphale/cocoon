#pragma once
#include "GraphicsHelper.h" 

class Scenes
{
private:
	GLuint vao;
	GLuint vbo_position;
	GLuint vbo_color;
	GLuint vbo_normal;
	GLuint vbo_texcoord;
	GLuint shaderProgramObject;

public:
	virtual void initialize(void) = 0;
	virtual void display(const mat4*) = 0;
	virtual void update(void) = 0;
	virtual void uninitialize(void) = 0;
};
