#pragma once
#include "Standard.h"
#include "GraphicsHelper.h"

#define DEFAULT_NUMBER_OF_SAMPLES 4

#define FRAMEBUFFER_WIDTH 3820
#define FRAMEBUFFER_HEIGHT 2160

class FrameBuffer
{
private:
	GLuint m_framebuffer;
	GLuint m_renderbuffer;
	GLuint m_numberOfSamples;

public:
	FrameBuffer() : m_framebuffer(0),
					m_renderbuffer(0),
					m_numberOfSamples(DEFAULT_NUMBER_OF_SAMPLES) { }

	~FrameBuffer() { }

	GLuint* GetFramebuffer() { return (&this->m_framebuffer); }
	void SetFramebuffer(GLuint framebuffer) { framebuffer = this->m_framebuffer; }
	
	GLuint* GenerateFrameBuffer(GLuint* frameBuffer = nullptr);
	GLuint* CreateFrameBuffer(GLuint* frameBuffer = nullptr, GLuint* colorTextureAttachment = nullptr, bool makeRenderBuffer = false);
	void BindFrameBuffer(const int framebufferEnum = GL_FRAMEBUFFER, const GLuint* frameBuffer = nullptr);
	void UnBindFrameBuffer(const int framebufferEnum = GL_FRAMEBUFFER);
	void DeleteFrameBuffer(GLuint* frameBuffer = nullptr);
};
