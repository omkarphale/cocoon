#pragma once
#include "Standard.h"
#include "GraphicsHelper.h"

class SceneBuffers
{

public:
	GLuint m_vao = 0;
	GLuint m_vbo_position = 0;
	GLuint m_vbo_color = 0;
	GLuint m_vbo_normal = 0;
	GLuint m_vbo_texcoord = 0;
	GLuint m_vbo_indices = 0;

	SceneBuffers() { }
	~SceneBuffers() { }

	void CreateSceneBuffers(SceneBuffers* buffer, GLuint* vao, GLuint* vbo, const int arraySize, const GLfloat* arrayValues, const int attributeLocation, const int stride, const GLenum usage, GLenum type);

	GLuint* CreateVertexArray(GLuint* vao);
	void BindVertexArray(const GLuint* vao);
	void UnBindVertexArray(void);
	void DeleteVertexArray(GLuint* vao);

	GLuint* CreateVertexBuffer(GLuint* vbo);
	void BindVertexBuffer(const GLuint* vbo, const int arraySize, const float* array, const int attribLocation, const int stride, const GLenum usage, GLenum type);
	void UnBindVertexBuffer(void);
	void DeleteVertexBuffer(GLuint* vbo);
};
