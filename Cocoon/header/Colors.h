#pragma once

class Colors
{
private:
	float m_red;
	float m_green;
	float m_blue;
	float m_alpha;

public:
	Colors(const float red, const float green, const float blue, const float alpha) : 
		m_red(red),
		m_green(green),
		m_blue(blue),
		m_alpha(alpha)
	{
	}
	
	~Colors() {};
};
