#pragma once
#include "Standard.h"
#include "GraphicsHelper.h"
#include "PerspectiveShapes.h"
#include "Framebuffer.h"
#include "ReferenecGrid.h"

class Renderer 
{
private:

public:
	float grayColor[4] = { 0.5f, 0.5f, 0.5f, 0.5f };
	float whiteColor[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
	float blackColor[4] = { 0.0f, 0.0f, 0.0f, 1.0f };
	float blueColor[4] = { 0.0f, 0.0f, 1.0f, 1.0f };
	
	void initializeRenderer(void);
	void displayRenderer(const mat4*);
	void updateRenderer(void);
	void uninitializeRenderer(void);
};
