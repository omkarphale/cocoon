#pragma once
#include "Standard.h"
#include "GraphicsHelper.h"
#include "LoadShaders.h"
#include "SceneBuffers.h"
#include "Logger.h"
#include "Scenes.h"

class ReferenecGrid : public Scenes
{
private:
	GLuint m_shaderProgramObject = 0;
	GLfloat m_rotationAngle = 0.0f;
	GLfloat m_rotationFactor = ROTATION_FACTOR; 
	
	SHADERS_INFO m_shaderInfo[2] = { 0 };

	std::vector<float> m_gridPositions;
	int m_vertex_count = 0;
	const int m_mesh_size = 1024;

public:
	ReferenecGrid();
	~ReferenecGrid();

	void initialize(void);
	void display(const mat4*);
	void update(void);
	void uninitialize(void);
};

