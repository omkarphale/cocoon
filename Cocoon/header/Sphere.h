#pragma once
#include <GraphicsHelper.h>
#include "Scenes.h"

class Sphere
{
private:
	float m_sphere_radius;

	std::vector<float> m_sphere_vertices;
	std::vector<float> m_sphere_normals;
	std::vector<float> m_sphere_texCoords;
	std::vector<unsigned int> m_sphere_indices;
	std::vector<unsigned int> m_sphere_lineIndices;

public:
	Sphere();
	Sphere(const float sphere_radius,
		   std::vector<float> sphere_vertices,
		   std::vector<float> sphere_normals,
		   std::vector<float> sphere_texCoords,
		   std::vector<unsigned int> sphere_indices,
		   std::vector<unsigned int> sphere_lineIndices);
	~Sphere();

	std::vector<float> const getPositionBuffers() { return (this->m_sphere_vertices); }
	std::vector<float> const getNormalsBuffers() { return (this->m_sphere_vertices); }
	std::vector<float> const getTexcoordsBuffers() { return (this->m_sphere_vertices); }
	std::vector<unsigned int> const getIndicesBuffers() { return (this->m_sphere_indices); }
	std::vector<unsigned int> const getLineIndicesBuffers() { return (this->m_sphere_lineIndices); }
	
	void MakeSphere(Sphere* sphere = nullptr, const float sphere_radius = 1.0f, const int stackCount = 100, const int sectorCount = 100);
	void DeleteSphere(Sphere* sphere = nullptr);
};
