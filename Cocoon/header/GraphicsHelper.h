#pragma once

// header files inclusion
#include <gl/glew.h>
#include <gl/GL.h>

#include "Logger.h"
#include "vmath.h"
#include "resource.h"

// pragma declaration
#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "opengl32.lib")

// macro declaration
// #define DEFAULT_FULLSCREEN
#define MAX_ROTATION_LIMIT 360.0f
#define ROTATION_FACTOR 1.0f

using namespace std;
using namespace vmath;

// 
enum
{
	COCOON_ATTRIBUTE_POSITION,
	COCOON_ATTRIBUTE_COLOR,
	COCOON_ATTRIBUTE_NORMAL,
	COCOON_ATTRIBUTE_TEXCOORD,
	COCOON_ATTRIBUTE_INDICES
};
