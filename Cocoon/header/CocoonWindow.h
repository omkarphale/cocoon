#pragma once
#include "Standard.h"
#include "GraphicsHelper.h"
#include "Renderer.h"
#include "Logger.h"

#define DEFAULT_WINDOW_WIDTH 800
#define DEFAULT_WINDOW_HEIGHT 600

#define LOG_MESSAGE_BOX(MESSAGE) MessageBox(m_hwnd, TEXT(MESSAGE), TEXT("Error"), MB_OK);

class CocoonWindow
{
private:
	
	MSG m_message;
	HDC m_hdc;
	HGLRC m_hrc;
	HWND m_hwnd;
	HINSTANCE m_hInstance;
	WNDCLASSEX m_wndclassex;

	bool m_fullScreen; 
	bool m_done;
	bool m_activeWindow;
	bool m_updateRendering;

	int m_cmdShow;
	int m_windowWidth;
	int m_windowHeight;

	const TCHAR szAppName[16] = TEXT("CocoonWindow"); 

	void ContextSetup(void);
	void DepthSettings(void);

public:

	// constructors
	CocoonWindow() { }
	CocoonWindow(HINSTANCE hInstance, MSG msg, int cmdShow);
	
	// destructor 
	~CocoonWindow() { }

	// getter 
	bool getActiveWindowStatus(void) { return(this->m_activeWindow); }
	bool getUpdateRenderingStatus(void) { return(this->m_updateRendering); }
	HWND getWindowHandle(void) { return(this->m_hwnd); }
	CocoonWindow& getCocoonWindow(void) { return(*this); }

	// setter
	void setActiveWindowStatus(const bool activeWindow) { this->m_activeWindow = activeWindow; }
	void setUpdateRenderingStatus(const bool updateRendering) { this->m_updateRendering = updateRendering; }
	void setWindowWidth(const int width) { this->m_windowWidth = width; }
	void setWindowHeight(const int height) { this->m_windowHeight = height; }
	
	// public functions
	void CocoonWindowGameLoop(void);
	void CocoonWindowToggleFullScreen(CocoonWindow& window);
	void CocoonWindowResizeWindow(int width, int height);
	void CocoonWindowInitialize(void);
	void CocoonWindowDisplay(void);
	void CocoonWindowUpdate(void);
	void CocoonWindowUninitialize(void);
};
