#pragma once
#include "Standard.h"

extern FILE* logFile;

#define DEBUG 0

#if DEBUG

    #define LogD(...)   if (!logFile)                                                                       \
                        {                                                                                   \
                            fopen_s(&logFile, "CocoonApplication.log", "a+");                               \
                        }                                                                                   \
                        if (logFile)                                                                        \
                        {                                                                                   \
                            fprintf(logFile, "%-0.8s | %s@%d ", __TIME__, __FILE__, __LINE__);              \
                            fprintf(logFile, "debug>> ");                                                   \
                            fprintf(logFile, __VA_ARGS__);                                                  \
                            fprintf(logFile, "\n");                                                         \
                            fclose(logFile);                                                                \
                            logFile = NULL;                                                                 \
                        }                                                                                   \

    #define LogI(...)   if (!logFile)                                                                       \
                        {                                                                                   \
                            fopen_s(&logFile, "CocoonApplication.log", "a+");                               \
                        }                                                                                   \
                        if (logFile)                                                                        \
                        {                                                                                   \
                            fprintf(logFile, "%-0.8s | %s@%d ", __TIME__, __FILE__, __LINE__);              \
                            fprintf(logFile, "info>> ");                                                    \
                            fprintf(logFile, __VA_ARGS__);                                                  \
                            fprintf(logFile, "\n");                                                         \
                            fclose(logFile);                                                                \
                            logFile = NULL;                                                                 \
                        }                                                                                   \

    #define LogE(...)   if (!logFile)                                                                       \
                        {                                                                                   \
                            fopen_s(&logFile, "CocoonApplication.log", "a+");                               \
                        }                                                                                   \
                        if (logFile)                                                                        \
                        {                                                                                   \
                            fprintf(logFile, "%-0.8s | %s@%d ", __TIME__, __FILE__, __LINE__);              \
                            fprintf(logFile, "error>> ");                                                   \
                            fprintf(logFile, __VA_ARGS__);                                                  \
                            fprintf(logFile, "\n");                                                         \
                            fclose(logFile);                                                                \
                            logFile = NULL;                                                                 \
                        }                                                                                   \

    #define LogStar(logtype)   if (!logFile)                                                                \
                               {                                                                            \
                                   fopen_s(&logFile, "CocoonApplication.log", "a+");                        \
                               }                                                                            \
                               if (logFile)                                                                 \
                               {                                                                            \
                                   fprintf(logFile, "%-0.8s | %s@%d ", __TIME__, __FILE__, __LINE__);       \
                                   fprintf(logFile, logtype);                                               \
                                   int count = 75;                                                          \
                                   while(count > 0)                                                         \
                                   {                                                                        \
                                       fprintf(logFile, "*");                                               \
                                       count = (count - 1);                                                 \
                                   }                                                                        \
                                   fprintf(logFile, "\n");                                                  \
                                   fclose(logFile);                                                         \
                                   logFile = NULL;                                                          \
                               }                                                                            \

#else
    
#define LogD(...)   if (!logFile)                                                                           \
                        {                                                                                   \
                            fopen_s(&logFile, "CocoonApplication.log", "a+");                               \
                        }                                                                                   \
                        if (logFile)                                                                        \
                        {                                                                                   \
                            fprintf(logFile, __VA_ARGS__);                                                  \
                            fprintf(logFile, "\n");                                                         \
                            fclose(logFile);                                                                \
                            logFile = NULL;                                                                 \
                        }                                                                                   \

#define LogI(...)   if (!logFile)                                                                           \
                        {                                                                                   \
                            fopen_s(&logFile, "CocoonApplication.log", "a+");                               \
                        }                                                                                   \
                        if (logFile)                                                                        \
                        {                                                                                   \
                            fprintf(logFile, __VA_ARGS__);                                                  \
                            fprintf(logFile, "\n");                                                         \
                            fclose(logFile);                                                                \
                            logFile = NULL;                                                                 \
                        }                                                                                   \

#define LogE(...)   if (!logFile)                                                                           \
                        {                                                                                   \
                            fopen_s(&logFile, "CocoonApplication.log", "a+");                               \
                        }                                                                                   \
                        if (logFile)                                                                        \
                        {                                                                                   \
                            fprintf(logFile, __VA_ARGS__);                                                  \
                            fprintf(logFile, "\n");                                                         \
                            fclose(logFile);                                                                \
                            logFile = NULL;                                                                 \
                        }                                                                                   \

#define LogStar(logtype)   if (!logFile)                                                                    \
                               {                                                                            \
                                   fopen_s(&logFile, "CocoonApplication.log", "a+");                        \
                               }                                                                            \
                               if (logFile)                                                                 \
                               {                                                                            \
                                   int count = 75;                                                          \
                                   while(count > 0)                                                         \
                                   {                                                                        \
                                       fprintf(logFile, "*");                                               \
                                       count = (count - 1);                                                 \
                                   }                                                                        \
                                   fprintf(logFile, "\n");                                                  \
                                   fclose(logFile);                                                         \
                                   logFile = NULL;                                                          \
                               }                                                                            \

#endif

#define LogGL() if (glGetError() != GL_NO_ERROR)                                             \
                {                                                                            \
                     LogE("glGetError() - %d", glGetError())                                 \
                }                                                                            \
                else                                                                         \
                {                                                                            \
                     LogI("glGetError() - %d", glGetError())                                 \
                }                                                                            \
                       
class Logger
{
public:
    Logger();
    ~Logger();
};
