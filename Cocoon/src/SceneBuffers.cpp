#include "SceneBuffers.h"
 
GLuint* SceneBuffers::CreateVertexArray(GLuint* vao)
{
    glGenVertexArrays(1, vao);

    return(vao);
}

GLuint* SceneBuffers::CreateVertexBuffer(GLuint* vbo)
{
    glGenBuffers(1, vbo);

    return(vbo);
}

void SceneBuffers::CreateSceneBuffers(SceneBuffers* buffer, GLuint* vao, GLuint* vbo, const int arraySize, const GLfloat* arrayValues, const int attributeLocation, const int stride, const GLenum usage, GLenum type)
{
    if (vao)
    {
        buffer->BindVertexArray(vao);

        buffer->CreateVertexBuffer(vbo);
        if (vbo)
        {
            buffer->BindVertexBuffer(vbo, arraySize, arrayValues, attributeLocation, stride, usage, type);
            buffer->UnBindVertexBuffer();
        }
        else
        {
            LogE("CreateVertexBuffer(vbo) is failed...")
        }
    }
}

void SceneBuffers::BindVertexArray(const GLuint* vao)
{
    glBindVertexArray(*vao);
}

void SceneBuffers::BindVertexBuffer(const GLuint* vbo, const int arraySize, const float* array, const int attribLocation, const int stride, const GLenum usage, GLenum type)
{
    glBindBuffer(GL_ARRAY_BUFFER, *vbo);
    glBufferData(GL_ARRAY_BUFFER, arraySize, array, usage);

    if(attribLocation != COCOON_ATTRIBUTE_INDICES)
    {
        glVertexAttribPointer(attribLocation, stride, type, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(attribLocation);
    }
}

void SceneBuffers::UnBindVertexArray(void)
{
    glBindVertexArray(0);
}

void SceneBuffers::UnBindVertexBuffer(void)
{
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void SceneBuffers::DeleteVertexArray(GLuint* vao)
{
    if (vao)
    {
        glDeleteBuffers(1, vao);
        vao = 0;
    }
}

void SceneBuffers::DeleteVertexBuffer(GLuint* vbo)
{
    if (vbo)
    {
        glDeleteBuffers(1, vbo);
        vbo = 0;
    }
}
