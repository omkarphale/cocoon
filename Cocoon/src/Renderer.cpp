#include "Renderer.h"

FrameBuffer* sceneFBO = new FrameBuffer();
FrameBuffer* postProcessingSceneFBO = new FrameBuffer();

PerspectiveShapes* perspectiveShapes = new PerspectiveShapes();
ReferenecGrid* referenecGrid = new ReferenecGrid();

void Renderer::initializeRenderer(void)
{
	perspectiveShapes->initialize();
	referenecGrid->initialize();

	GLuint sceneTexture;
	sceneFBO->GenerateFrameBuffer(sceneFBO->GetFramebuffer());
	sceneFBO->CreateFrameBuffer(sceneFBO->GetFramebuffer(), &sceneTexture, true);

	postProcessingSceneFBO->GenerateFrameBuffer(postProcessingSceneFBO->GetFramebuffer());
	postProcessingSceneFBO->CreateFrameBuffer(postProcessingSceneFBO->GetFramebuffer(), &sceneTexture);
}

void Renderer::displayRenderer(const mat4* perspectiveMatrix)
{
	sceneFBO->BindFrameBuffer(GL_FRAMEBUFFER, sceneFBO->GetFramebuffer());
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	perspectiveShapes->display(perspectiveMatrix);
	referenecGrid->display(perspectiveMatrix);
	
	sceneFBO->UnBindFrameBuffer(GL_FRAMEBUFFER);

	sceneFBO->BindFrameBuffer(GL_READ_FRAMEBUFFER, sceneFBO->GetFramebuffer());
	sceneFBO->UnBindFrameBuffer(GL_DRAW_FRAMEBUFFER);
	glBlitFramebuffer(0, 0, FRAMEBUFFER_WIDTH, FRAMEBUFFER_HEIGHT, 0, 0, FRAMEBUFFER_WIDTH, FRAMEBUFFER_HEIGHT, GL_COLOR_BUFFER_BIT, GL_NEAREST);
}

void Renderer::updateRenderer(void)
{
	perspectiveShapes->update();
	referenecGrid->update();
}

void Renderer::uninitializeRenderer(void)
{
	if (sceneFBO->GetFramebuffer())
	{
		sceneFBO->DeleteFrameBuffer(sceneFBO->GetFramebuffer());
	}

	if (postProcessingSceneFBO->GetFramebuffer())
	{
		postProcessingSceneFBO->DeleteFrameBuffer(postProcessingSceneFBO->GetFramebuffer());
	}

	perspectiveShapes->uninitialize();
	referenecGrid->uninitialize();
}
