#include "PerspectiveShapes.h"

SceneBuffers* multiColoredQuadBuffers = new SceneBuffers();
SceneBuffers* multiColoredTriangleBuffers = new SceneBuffers();
SceneBuffers* sphereBuffers = new SceneBuffers();

Sphere* sphere = new Sphere();

extern mat4 perspectiveProjectionMatrix;

PerspectiveShapes::PerspectiveShapes()
{
}

PerspectiveShapes::~PerspectiveShapes()
{
}

void PerspectiveShapes::CreateShapeBuffers(SceneBuffers* buffer, GLuint* vao, GLuint* vbo, const int arraySize, const GLfloat* arrayValues, const int attributeLocation, const int stride, const GLenum usage, const GLenum type)
{
	if (buffer == sphereBuffers)
	{
		buffer->CreateSceneBuffers(buffer, vao, vbo, arraySize, arrayValues, attributeLocation, stride, usage, GL_UNSIGNED_INT);
	}
	else
	{
		buffer->CreateSceneBuffers(buffer, vao, vbo, arraySize, arrayValues, attributeLocation, stride, usage, type);
	}
}

void PerspectiveShapes::initialize(void)
{
	this->m_shaderInfo[0].shaderType = GL_VERTEX_SHADER;
	this->m_shaderInfo[0].shaderLoadAs = CREATE_PROGRAM_SHADER_LOAD_FROM_FILE;
	this->m_shaderInfo[0].shaderFileName = "./res/shaders/PerspectiveShapes/PerspectiveShapes.vert";
	this->m_shaderInfo[0].shaderID = 0;

	this->m_shaderInfo[1].shaderType = GL_FRAGMENT_SHADER;
	this->m_shaderInfo[1].shaderLoadAs = CREATE_PROGRAM_SHADER_LOAD_FROM_FILE;
	this->m_shaderInfo[1].shaderFileName = "./res/shaders/PerspectiveShapes/PerspectiveShapes.frag";
	this->m_shaderInfo[1].shaderID = 0;

	BIND_ATTRIBUTES_INFO m_modelAttributesInfo[] = { {"v_position", COCOON_ATTRIBUTE_POSITION}, {"v_color", COCOON_ATTRIBUTE_COLOR} };

	this->m_shaderProgramObject = createProgram(m_shaderInfo, _ARRAYSIZE(m_shaderInfo), m_modelAttributesInfo, _ARRAYSIZE(m_modelAttributesInfo), __LINE__);
	if (this->m_shaderProgramObject == 0)
	{
		LogE("CreateProgram(this->m_shaderProgramObject) failed.");
		this->uninitialize();
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// triangle
	multiColoredTriangleBuffers->CreateVertexArray(&multiColoredTriangleBuffers->m_vao);
	this->CreateShapeBuffers(multiColoredTriangleBuffers, &multiColoredTriangleBuffers->m_vao, &multiColoredTriangleBuffers->m_vbo_position, sizeof(this->triangleVertices), this->triangleVertices, COCOON_ATTRIBUTE_POSITION, 3);
	this->CreateShapeBuffers(multiColoredTriangleBuffers, &multiColoredTriangleBuffers->m_vao, &multiColoredTriangleBuffers->m_vbo_color, sizeof(this->triangleColors), this->triangleColors, COCOON_ATTRIBUTE_COLOR, 3);
	
	// quad
	multiColoredQuadBuffers->CreateVertexArray(&multiColoredQuadBuffers->m_vao);
	this->CreateShapeBuffers(multiColoredQuadBuffers, &multiColoredQuadBuffers->m_vao, &multiColoredQuadBuffers->m_vbo_position, sizeof(this->quadVertices), this->quadVertices, COCOON_ATTRIBUTE_POSITION, 3);
	this->CreateShapeBuffers(multiColoredQuadBuffers, &multiColoredQuadBuffers->m_vao, &multiColoredQuadBuffers->m_vbo_color, sizeof(this->quadColors), this->quadColors, COCOON_ATTRIBUTE_COLOR, 3);

	// sphere
	// sphere->MakeSphere(sphere, 1.0f, 30, 30);
	// this->CreateShapeBuffers(sphereBuffers, &sphereBuffers->m_vao, &sphereBuffers->m_vbo_position, sphere->getPositionBuffers().size(), sphere->getPositionBuffers().data(), COCOON_ATTRIBUTE_POSITION, 3);
	// this->CreateShapeBuffers(sphereBuffers, &sphereBuffers->m_vao, &sphereBuffers->m_vbo_indices, sphere->getIndicesBuffers().size(), sphere->getIndicesBuffers().data(), COCOON_ATTRIBUTE_INDICES, 3, GL_STATIC_DRAW, GL_UNSIGNED_INT);

	LogI("PerspectiveShapes::initialized all the buffers and shader program")
}

void PerspectiveShapes::DrawShapesUsingArray(SceneBuffers* buffer, const int count, const mat4 viewMatrix, const mat4 modelMatrix, const mat4 projectionMatrix)
{
	// OpenGL Animation
	glUniformMatrix4fv(glGetUniformLocation(this->m_shaderProgramObject, "u_projection_matrix"), 1, GL_FALSE, projectionMatrix);
	glUniformMatrix4fv(glGetUniformLocation(this->m_shaderProgramObject, "u_view_matrix"), 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(glGetUniformLocation(this->m_shaderProgramObject, "u_model_matrix"), 1, GL_FALSE, modelMatrix);

	buffer->BindVertexArray(&buffer->m_vao);
		glDrawArrays(GL_TRIANGLES, 0, count);
	buffer->UnBindVertexArray();
}

void PerspectiveShapes::DrawShapesUsingElements(SceneBuffers* buffer, const int count, const mat4 viewMatrix, const mat4 modelMatrix, const mat4 projectionMatrix)
{
	// OpenGL Animation
	glUniformMatrix4fv(glGetUniformLocation(this->m_shaderProgramObject, "u_projection_matrix"), 1, GL_FALSE, projectionMatrix);
	glUniformMatrix4fv(glGetUniformLocation(this->m_shaderProgramObject, "u_view_matrix"), 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(glGetUniformLocation(this->m_shaderProgramObject, "u_model_matrix"), 1, GL_FALSE, modelMatrix);

	buffer->BindVertexArray(&buffer->m_vao);
		glDrawElements(GL_TRIANGLES, count, GL_UNSIGNED_INT, 0);
	buffer->UnBindVertexArray();
}

void PerspectiveShapes::display(const mat4* perspectiveProjectionMatrix)
{
	glUseProgram(this->m_shaderProgramObject);

	mat4 viewMatrix = mat4::identity();
	mat4 modelMatrix = mat4::identity();

	// triangle
	modelMatrix = vmath::translate(-2.0f, 0.0f, -6.0f);
	modelMatrix = modelMatrix * vmath::rotate(this->m_rotationAngle, 0.0f, 1.0f, 0.0f);
	this->DrawShapesUsingArray(multiColoredTriangleBuffers, sizeof(triangleVertices) / sizeof(GLfloat), viewMatrix, modelMatrix, *perspectiveProjectionMatrix);

	// quad
	modelMatrix = mat4::identity();
	modelMatrix = vmath::translate(+2.0f, 0.0f, -6.0f);
	modelMatrix = modelMatrix * vmath::rotate(this->m_rotationAngle, 1.0f, 0.0f, 0.0f);
	this->DrawShapesUsingArray(multiColoredQuadBuffers, sizeof(quadVertices) / sizeof(GLfloat), viewMatrix, modelMatrix, *perspectiveProjectionMatrix);
	
	// sphere
	/*modelMatrix = mat4::identity();
	modelMatrix = vmath::translate(0.0f, 0.5f, -6.0f);
	modelMatrix = modelMatrix * vmath::rotate(this->m_rotationAngle, 0.0f, 1.0f, 0.0f);
	this->DrawShapesUsingElements(sphereBuffers, sphere->getIndicesBuffers().size(), viewMatrix, modelMatrix, *perspectiveProjectionMatrix);*/

	glUseProgram(0);
}

void PerspectiveShapes::update(void)
{
	this->m_rotationAngle = this->m_rotationAngle + this->m_rotationFactor;
	if (this->m_rotationAngle >= MAX_ROTATION_LIMIT)
	{
		this->m_rotationAngle = MAX_ROTATION_LIMIT - this->m_rotationAngle;
	}
}

void PerspectiveShapes::uninitialize(void)
{
	multiColoredTriangleBuffers->DeleteVertexArray(&multiColoredTriangleBuffers->m_vao);
	multiColoredTriangleBuffers->DeleteVertexBuffer(&multiColoredTriangleBuffers->m_vbo_position);
	multiColoredTriangleBuffers->DeleteVertexBuffer(&multiColoredTriangleBuffers->m_vbo_color);
	multiColoredTriangleBuffers->DeleteVertexBuffer(&multiColoredTriangleBuffers->m_vbo_normal);
	multiColoredTriangleBuffers->DeleteVertexBuffer(&multiColoredTriangleBuffers->m_vbo_texcoord);

	deleteProgram(this->m_shaderProgramObject);

	LogI("PerspectiveShapes::uninitialized all the buffers and shader program")
}
