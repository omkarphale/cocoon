#include "Framebuffer.h"

GLuint* FrameBuffer::GenerateFrameBuffer(GLuint* frameBuffer)
{
	glGenFramebuffers(1, &m_framebuffer);

	return (frameBuffer);
}

GLuint* FrameBuffer::CreateFrameBuffer(GLuint* frameBuffer, 
									   GLuint* colorTextureAttachment,
									   bool makeRenderBuffer)
{
	this->BindFrameBuffer(GL_FRAMEBUFFER, frameBuffer); 

	glGenTextures(1, colorTextureAttachment);
	glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, *colorTextureAttachment);
	glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, this->m_numberOfSamples, GL_RGB, FRAMEBUFFER_WIDTH, FRAMEBUFFER_HEIGHT, GL_TRUE);
	glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE, *colorTextureAttachment, 0);

	if (makeRenderBuffer)
	{
		glGenRenderbuffers(1, &this->m_renderbuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, this->m_renderbuffer);
		glRenderbufferStorageMultisample(GL_RENDERBUFFER, this->m_numberOfSamples, GL_DEPTH24_STENCIL8, FRAMEBUFFER_WIDTH, FRAMEBUFFER_HEIGHT);
		glBindRenderbuffer(GL_RENDERBUFFER, 0);

		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, this->m_renderbuffer);
	}

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{		
		LogE("FRAMEBUFFER:: Framebuffer is not complete!");
	}

	this->UnBindFrameBuffer(GL_FRAMEBUFFER);

	return(frameBuffer);
}

void FrameBuffer::BindFrameBuffer(const int framebufferEnum, 
								  const GLuint* frameBuffer)
{
	glBindFramebuffer(framebufferEnum, this->m_framebuffer);
}

void FrameBuffer::UnBindFrameBuffer(const int framebufferEnum)
{
	glBindFramebuffer(framebufferEnum, 0);
}

void FrameBuffer::DeleteFrameBuffer(GLuint* frameBuffer)
{
	if(frameBuffer)
	{
		glDeleteFramebuffers(1, frameBuffer);
		frameBuffer = nullptr;
	}
}
