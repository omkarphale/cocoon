#include "ReferenecGrid.h"

SceneBuffers* referenceGridBuffers = new SceneBuffers();

ReferenecGrid::ReferenecGrid()
{	
	m_gridPositions.clear();
}

ReferenecGrid::~ReferenecGrid()
{
}

void ReferenecGrid::initialize(void)
{
	this->m_shaderInfo[0].shaderType = GL_VERTEX_SHADER;
	this->m_shaderInfo[0].shaderLoadAs = CREATE_PROGRAM_SHADER_LOAD_FROM_FILE;
	this->m_shaderInfo[0].shaderFileName = "./res/shaders/RefernceGrid/RefernceGrid.vert";
	this->m_shaderInfo[0].shaderID = 0;

	this->m_shaderInfo[1].shaderType = GL_FRAGMENT_SHADER;
	this->m_shaderInfo[1].shaderLoadAs = CREATE_PROGRAM_SHADER_LOAD_FROM_FILE;
	this->m_shaderInfo[1].shaderFileName = "./res/shaders/RefernceGrid/RefernceGrid.frag";
	this->m_shaderInfo[1].shaderID = 0;

	BIND_ATTRIBUTES_INFO m_modelAttributesInfo[] = { {"v_position", COCOON_ATTRIBUTE_POSITION} };

	this->m_shaderProgramObject = createProgram(this->m_shaderInfo, _ARRAYSIZE(m_shaderInfo), m_modelAttributesInfo, _ARRAYSIZE(m_modelAttributesInfo), __LINE__);
	if (this->m_shaderProgramObject == 0)
	{
		LogE("CreateProgram(this->m_shaderProgramObject) failed.");
		this->uninitialize();
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////

	for (int width = 0; width <= this->m_mesh_size; ++width)
	{
		m_gridPositions.push_back(float(width - this->m_mesh_size / 2.0));
		m_gridPositions.push_back(0.0f);
		m_gridPositions.push_back(float(-this->m_mesh_size / 2.0f));

		m_gridPositions.push_back(float(width - this->m_mesh_size / 2.0));
		m_gridPositions.push_back(0.0f);
		m_gridPositions.push_back((this->m_mesh_size / 2.0f));
	}

	for (int depth = 0; depth <= this->m_mesh_size; ++depth)
	{
		m_gridPositions.push_back(float(-this->m_mesh_size / 2.0f));
		m_gridPositions.push_back(0.0f);
		m_gridPositions.push_back(float(depth - this->m_mesh_size / 2.0));

		m_gridPositions.push_back(float(this->m_mesh_size / 2.0f));
		m_gridPositions.push_back(0.0f);
		m_gridPositions.push_back(float(depth - this->m_mesh_size / 2.0));
	}

	this->m_vertex_count = (m_gridPositions.size() / 3);

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////

	referenceGridBuffers->CreateVertexArray(&referenceGridBuffers->m_vao);
	referenceGridBuffers->CreateSceneBuffers(referenceGridBuffers, &referenceGridBuffers->m_vao, &referenceGridBuffers->m_vbo_position, sizeof(float) * this->m_gridPositions.size(), &this->m_gridPositions[0], COCOON_ATTRIBUTE_POSITION, 3, GL_STATIC_DRAW, GL_FLOAT);
}

void ReferenecGrid::display(const mat4* projectionMatrix)
{
	glUseProgram(this->m_shaderProgramObject);

		mat4 viewMatrix = mat4::identity();
		mat4 modelMatrix = mat4::identity();

		modelMatrix = vmath::translate(0.0f, -6.0f, -512.0f);
		
		glUniformMatrix4fv(glGetUniformLocation(this->m_shaderProgramObject, "u_projection_matrix"), 1, GL_FALSE, *projectionMatrix);
		glUniformMatrix4fv(glGetUniformLocation(this->m_shaderProgramObject, "u_view_matrix"), 1, GL_FALSE, viewMatrix);
		glUniformMatrix4fv(glGetUniformLocation(this->m_shaderProgramObject, "u_model_matrix"), 1, GL_FALSE, modelMatrix);

		referenceGridBuffers->BindVertexArray(&referenceGridBuffers->m_vao);
			glDrawArrays(GL_LINES, 0, this->m_vertex_count);
		referenceGridBuffers->UnBindVertexArray();

	glUseProgram(0);
}

void ReferenecGrid::update(void)
{
	this->m_rotationAngle = this->m_rotationAngle + this->m_rotationFactor;
	if (this->m_rotationAngle >= MAX_ROTATION_LIMIT)
	{
		this->m_rotationAngle = MAX_ROTATION_LIMIT - this->m_rotationAngle;
	}
}

void ReferenecGrid::uninitialize(void)
{
}
