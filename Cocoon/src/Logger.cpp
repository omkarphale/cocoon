// header files
#include <Windows.h>
#include <stdio.h>

#include "Logger.h"

// globals
FILE* logFile;

Logger::Logger()
{
	// open file for logging
	if (fopen_s(&logFile, "CocoonApplication.log", "w+") != 0)
	{
		MessageBox(NULL, TEXT("failed to open CocoonApplication.log file..."), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
}

Logger::~Logger()
{
    if (logFile)                                                                        
    {                                                                                   
        fclose(logFile);                                                                
        logFile = NULL;                                                                 
    }
}
