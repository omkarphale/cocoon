#include "CocoonWindow.h"

int WINAPI WinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpszCmdLine, _In_ int iCmdShow)
{
	MSG message = {};
	CocoonWindow CocoonWindow(hInstance, message, iCmdShow);

	CocoonWindow.CocoonWindowGameLoop();

	return ((int)message.wParam);
}
