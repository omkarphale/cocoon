#include "Sphere.h"

Sphere::Sphere() 
{
}

Sphere::Sphere(const float sphere_radius,
    std::vector<float> sphere_vertices,
    std::vector<float> sphere_normals,
    std::vector<float> sphere_texCoords,
    std::vector<unsigned int> sphere_indices,
    std::vector<unsigned int> sphere_lineIndices) : m_sphere_radius(sphere_radius),
                                                    m_sphere_vertices(sphere_vertices),
                                                    m_sphere_normals(sphere_normals),
                                                    m_sphere_texCoords(sphere_texCoords),
                                                    m_sphere_indices(sphere_indices),
                                                    m_sphere_lineIndices(sphere_lineIndices)
{
}

void Sphere::MakeSphere(Sphere* sphere, const float sphere_radius, const int stackCount, const int sectorCount)
{    
    sphere->m_sphere_vertices.clear();
    sphere->m_sphere_normals.clear();
    sphere->m_sphere_texCoords.clear();

    std::vector<float>().swap(sphere->m_sphere_vertices);
    std::vector<float>().swap(sphere->m_sphere_normals);
    std::vector<float>().swap(sphere->m_sphere_texCoords);

    float x, y, z, xy;                          
    float nx, ny, nz, lengthInv = 1.0f / sphere_radius; 
    float s, t;                               

    float sectorStep = 2 * M_PI / sectorCount;
    float stackStep = M_PI / stackCount;
    float sectorAngle, stackAngle;

    for (int i = 0; i <= stackCount; ++i)
    {
        stackAngle = M_PI / 2 -(i * stackStep); 
        xy = sphere_radius * cosf(stackAngle);       
        z = sphere_radius * sinf(stackAngle);       

        for (int j = 0; j <= sectorCount; ++j)
        {
            sectorAngle = j * sectorStep; 

            x = xy * cosf(sectorAngle);
            y = xy * sinf(sectorAngle); 
            sphere->m_sphere_vertices.push_back(x);
            sphere->m_sphere_vertices.push_back(y);
            sphere->m_sphere_vertices.push_back(z);

            nx = x * lengthInv;
            ny = y * lengthInv;
            nz = z * lengthInv;
            sphere->m_sphere_normals.push_back(nx);
            sphere->m_sphere_normals.push_back(ny);
            sphere->m_sphere_normals.push_back(nz);

             s = (float)j / sectorCount;
            t = (float)i / stackCount;

            sphere->m_sphere_texCoords.push_back(-s);
            sphere->m_sphere_texCoords.push_back(t);
        }
    }

    int k1, k2;
    for (int i = 0; i < stackCount; ++i)
    {
        k1 = i * (sectorCount + 1); 
        k2 = k1 + sectorCount + 1;  

        for (int j = 0; j < sectorCount; ++j, ++k1, ++k2)
        {
            if (i != 0)
            {
                sphere->m_sphere_indices.push_back(k1);
                sphere->m_sphere_indices.push_back(k2);
                sphere->m_sphere_indices.push_back(k1 + 1);
            }

            if (i != (stackCount - 1))
            {
                sphere->m_sphere_indices.push_back(k1 + 1);
                sphere->m_sphere_indices.push_back(k2);
                sphere->m_sphere_indices.push_back(k2 + 1);
            }

            sphere->m_sphere_lineIndices.push_back(k1);
            sphere->m_sphere_lineIndices.push_back(k2);
            if (i != 0) 
            {
                sphere->m_sphere_lineIndices.push_back(k1);
                sphere->m_sphere_lineIndices.push_back(k1 + 1);
            }
        }
    }
}

void Sphere::DeleteSphere(Sphere* sphere)
{
    if(sphere->m_sphere_vertices.size())
    {
        sphere->m_sphere_vertices.clear();
    }

    if (sphere->m_sphere_normals.size())
    {
        sphere->m_sphere_normals.clear();
    }

    if (sphere->m_sphere_texCoords.size())
    {
        sphere->m_sphere_texCoords.clear();
    }

    if (sphere)
    {
        delete(sphere);
        sphere = nullptr;
    }
}

Sphere::~Sphere()
{
}
