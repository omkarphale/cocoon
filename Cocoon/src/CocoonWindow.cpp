#include "CocoonWindow.h"

// callback function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variables
Logger* logger = new Logger();
Renderer* renderer = new Renderer();
CocoonWindow* cocoonWindow = new CocoonWindow();

mat4 perspectiveProjectionMatrix;

CocoonWindow::CocoonWindow(HINSTANCE hInstance, MSG msg, int cmdShow) : m_hdc(nullptr),
																		m_hwnd(nullptr),
																		m_hInstance(hInstance),
																		m_message(msg),
																		m_cmdShow(cmdShow),
																		m_windowWidth(DEFAULT_WINDOW_WIDTH),
																		m_windowHeight(DEFAULT_WINDOW_HEIGHT),
																		m_fullScreen(false),
																		m_done(false),
																		m_activeWindow(false),
																		m_updateRendering(false)
{
	// Initialization Of m_wndclassex
	this->m_wndclassex.cbSize = sizeof(m_wndclassex);
	this->m_wndclassex.style = CS_HREDRAW | CS_VREDRAW;
	this->m_wndclassex.cbClsExtra = 0;
	this->m_wndclassex.cbWndExtra = 0;
	this->m_wndclassex.lpfnWndProc = WndProc;
	this->m_wndclassex.hInstance = hInstance;
	this->m_wndclassex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(COCOON_ICON));
	this->m_wndclassex.hCursor = LoadCursor(NULL, IDC_ARROW);
	this->m_wndclassex.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	this->m_wndclassex.lpszClassName = this->szAppName;
	this->m_wndclassex.lpszMenuName = NULL;
	this->m_wndclassex.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(COCOON_ICON));

	// Register Above Class
	RegisterClassEx(&this->m_wndclassex);

	int iScreenWidthX = GetSystemMetrics(SM_CXSCREEN);
	int iScreenHeightY = GetSystemMetrics(SM_CYSCREEN);
	int iCenterX = (iScreenWidthX / 2) - (this->m_windowWidth / 2);
	int iCenterY = (iScreenHeightY / 2) - (this->m_windowHeight / 2);

	// Create Window
	this->m_hwnd = CreateWindow(this->szAppName,
		TEXT("Cocoon"),
		WS_OVERLAPPEDWINDOW,
		iCenterX,
		iCenterY,
		this->m_windowWidth,
		this->m_windowHeight,
		NULL,
		NULL,
		this->m_hInstance,
		NULL);

	LogI("|| SHREE ||\n");
	LogI("Cocoon :: Application Started");
	LogStar("info>> ");

	CocoonWindowInitialize();

	cocoonWindow = this;
}

void CocoonWindow::CocoonWindowGameLoop(void)
{
	// Show And Update Window
	ShowWindow(this->m_hwnd, this->m_cmdShow);

	SetForegroundWindow(this->m_hwnd);
	SetFocus(this->m_hwnd);

	while (m_done == false)
	{
		if (PeekMessage(&m_message, NULL, 0, 0, PM_REMOVE))
		{
			if (m_message.message == WM_QUIT)
			{
				m_done = true;
			}
			else
			{
				TranslateMessage(&m_message);
				DispatchMessage(&m_message);
			}
		}
		else
		{
			if (m_activeWindow == true)
			{
				cocoonWindow->CocoonWindowDisplay();

				if (m_updateRendering == true)
				{
					cocoonWindow->CocoonWindowUpdate();
				}
			}
		}
	}
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	//Code
	switch (message)
	{
	case WM_CREATE:
#ifdef DEFAULT_FULLSCREEN
		cocoonWindow->CocoonWindowToggleFullScreen(*cocoonWindow);
#endif
		break;
	
	case WM_SETFOCUS:
		cocoonWindow->setActiveWindowStatus(true);
		break;

	case WM_KILLFOCUS:
		cocoonWindow->setActiveWindowStatus(false);
		break;

	case WM_ERASEBKGND:
		return (0);

	case WM_SIZE:
		cocoonWindow->CocoonWindowResizeWindow(LOWORD(lParam), HIWORD(lParam));

		cocoonWindow->setWindowWidth(LOWORD(lParam));
		cocoonWindow->setWindowHeight(HIWORD(lParam));
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:			
			DestroyWindow(hwnd);
			break;

		default:
			break;
		}
		break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'F': 
		case 'f':			
			cocoonWindow->CocoonWindowToggleFullScreen(*cocoonWindow);
			break;

		case 'P':
		case 'p':
			(cocoonWindow->getUpdateRenderingStatus() == true) ? (cocoonWindow->setUpdateRenderingStatus(false)) : (cocoonWindow->setUpdateRenderingStatus(true));
			break;

		default:
			break;
		}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		cocoonWindow->CocoonWindowUninitialize();
		PostQuitMessage(0);
		break;

	default:
		break;
	}

	return(DefWindowProc(hwnd, message, wParam, lParam));
}

void CocoonWindow::CocoonWindowToggleFullScreen(CocoonWindow& window)
	{
	// Local Variable Declaration
	static DWORD dwStyle;
	MONITORINFO monitorinfo;
	static WINDOWPLACEMENT wpPrev;

	// Code
	wpPrev.length = sizeof(WINDOWPLACEMENT);
	if (window.m_fullScreen == false)
	{
		dwStyle = GetWindowLong(window.m_hwnd, GWL_STYLE); // GWL_STYLE - Get Window Long
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			monitorinfo.cbSize = sizeof(MONITORINFO);
			if (GetWindowPlacement(window.m_hwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(window.m_hwnd, MONITORINFOF_PRIMARY), &monitorinfo))
			{
				SetWindowLong(window.m_hwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(window.m_hwnd,														 //
					HWND_TOP,													 //
					monitorinfo.rcMonitor.left,								 //
					monitorinfo.rcMonitor.top,									 //
					(monitorinfo.rcMonitor.right - monitorinfo.rcMonitor.left), //
					(monitorinfo.rcMonitor.bottom - monitorinfo.rcMonitor.top), //
					SWP_NOZORDER | SWP_FRAMECHANGED);							 // SWP_FRAMECHANGED sends WM_NCCALCSIZE, SWP stands for SetWindowPos
			}

			ShowCursor(FALSE);
			window.m_fullScreen = true;
		}
	}

	else
	{
		SetWindowLong(window.m_hwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(window.m_hwnd, &wpPrev);
		SetWindowPos(window.m_hwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
		window.m_fullScreen = false;
	}
}

void CocoonWindow::CocoonWindowResizeWindow(int width, int height)
{
	if (height <= 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f,
								(GLfloat)width / (GLfloat)height,
								0.1f,
								10000.0f);
}

void CocoonWindow::CocoonWindowInitialize(void)
{
	ContextSetup();

	renderer->initializeRenderer();
	
	DepthSettings();

	perspectiveProjectionMatrix = mat4::identity();
	CocoonWindowResizeWindow(this->m_windowWidth, this->m_windowHeight);
}

void CocoonWindow::CocoonWindowDisplay(void)
{	
	renderer->displayRenderer(&perspectiveProjectionMatrix);

	SwapBuffers(this->m_hdc);
}

void CocoonWindow::CocoonWindowUpdate(void)
{
	renderer->updateRenderer();
}

void CocoonWindow::CocoonWindowUninitialize(void)
{
	renderer->uninitializeRenderer();

	LogStar("info>> ");
	LogI("Cocoon :: Application Terminated");
	LogI("Created By Omkar Phale");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//												Helper Functions 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
void CocoonWindow::ContextSetup(void)

{	// local variable declaration
	PIXELFORMATDESCRIPTOR pixelformatdescriptor;
	int iIndex = 0;
	int iPixelFormatIndex = 0;
	GLenum glew_error = 0;

	int numberOfExtensions = 0;
	int numberOfSamples = 0;

	// getting the device context
	m_hdc = GetDC(m_hwnd);
	if (!m_hdc)
	{
		LOG_MESSAGE_BOX("m_hdc")
	}

	// initialization of pixelFormatDescriptor
	ZeroMemory(&pixelformatdescriptor, sizeof(pixelformatdescriptor));

	pixelformatdescriptor.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pixelformatdescriptor.nVersion = 1;
	pixelformatdescriptor.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pixelformatdescriptor.iPixelType = PFD_TYPE_RGBA;
	pixelformatdescriptor.cColorBits = 32;
	pixelformatdescriptor.cRedBits = 8;
	pixelformatdescriptor.cGreenBits = 8;
	pixelformatdescriptor.cBlueBits = 8;
	pixelformatdescriptor.cAlphaBits = 8;
	pixelformatdescriptor.cDepthBits = 32;

	iPixelFormatIndex = ChoosePixelFormat(m_hdc, &pixelformatdescriptor);
	if (iPixelFormatIndex == 0)
	{
		LogE("iPixelFormatIndex is invalid")
		this->CocoonWindowUninitialize();
	}

	if (SetPixelFormat(m_hdc, iPixelFormatIndex, &pixelformatdescriptor) == FALSE)
	{
		LogE("SetPixelFormat() is invalid")
		this->CocoonWindowUninitialize();
	}

	m_hrc = wglCreateContext(m_hdc);
	if (m_hrc == NULL)
	{
		LogE("wglCreateContext() is invalid")
		this->CocoonWindowUninitialize();
	}

	if (wglMakeCurrent(m_hdc, m_hrc) == FALSE)
	{
		LogE("wglMakeCurrent() is invalid")
		this->CocoonWindowUninitialize();
	}

	glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		LogE("glewInit() is invalid")
		this->CocoonWindowUninitialize();
	}

	LogI("[=================== OpenGL Related Specification Log ===================]");
	LogI("OpenGL Vendor : %s", glGetString(GL_VENDOR));
	LogI("OpenGL Renderer : %s", glGetString(GL_RENDERER));
	LogI("OpenGL Version : %s", glGetString(GL_VERSION));
	LogI("GLSL(Graphics Library Shading Language) Version : %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	//glGetIntegerv(GL_NUM_EXTENSIONS, &numberOfExtensions);
	//LogI("Extensions :")
	//for (iIndex = 0; iIndex < numberOfExtensions; iIndex++)
	//{
	//	LogI("%3d] %s", iIndex, glGetStringi(GL_EXTENSIONS, iIndex));
	//}
}

void CocoonWindow::DepthSettings(void)
{
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(renderer->grayColor[0], renderer->grayColor[1], renderer->grayColor[2], renderer->grayColor[3]);
}
